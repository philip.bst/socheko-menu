let maincategory = document.getElementsByClassName('main-category');
let subcategory = document.getElementsByClassName('sub-category');
let active = false;

for(let index=0;index<maincategory.length;index++){
    maincategory[index].addEventListener('click',()=>{
        subcategory[index].style.display = "block";
        active = true;
    })
}

let subCategoryList = document.querySelectorAll(".sub-category ul li");

for(let i = 0;i<subCategoryList.length;i++){
    subCategoryList[i].removeAttribute('style');
}

let rowLayout = document.querySelectorAll('ps-tabs > ps-tab.active');
for(let i=0;i<rowLayout.length;i++){
    rowLayout[i].classList.remove = "active";
}